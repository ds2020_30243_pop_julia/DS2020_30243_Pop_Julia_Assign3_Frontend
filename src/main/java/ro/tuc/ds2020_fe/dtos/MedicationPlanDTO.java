package ro.tuc.ds2020_fe.dtos;

import java.util.List;

public class MedicationPlanDTO {


    private String intakeIntervals;

    private String treatmentPeriod;

    private String doctorName;

    private String patientName;

    private List<String> medicationNames;

    public MedicationPlanDTO(String intakeIntervals, String treatmentPeriod, String doctorName, String patientName, List<String> medicationNames) {
        this.intakeIntervals = intakeIntervals;
        this.treatmentPeriod = treatmentPeriod;
        this.doctorName = doctorName;
        this.patientName = patientName;
        this.medicationNames = medicationNames;
    }

    public MedicationPlanDTO() {
		// TODO Auto-generated constructor stub
	}

	public String getIntakeIntervals() {
        return intakeIntervals;
    }

    public void setIntakeIntervals(String intakeIntervals) {
        this.intakeIntervals = intakeIntervals;
    }

    public String getTreatmentPeriod() {
        return treatmentPeriod;
    }

    public void setTreatmentPeriod(String treatmentPeriod) {
        this.treatmentPeriod = treatmentPeriod;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public List<String> getMedicationNames() {
        return medicationNames;
    }

    public void setMedicationNames(List<String> medicationName) {
        this.medicationNames = medicationName;
    }

    @Override
    public String toString() {
        return "MedicationPlanDTO{" +
                "intakeIntervals='" + intakeIntervals + '\'' +
                ", treatmentPeriod='" + treatmentPeriod + '\'' +
                ", doctorName='" + doctorName + '\'' +
                ", patientName='" + patientName + '\'' +
                ", medicationNames=" + medicationNames +
                '}';
    }
}
