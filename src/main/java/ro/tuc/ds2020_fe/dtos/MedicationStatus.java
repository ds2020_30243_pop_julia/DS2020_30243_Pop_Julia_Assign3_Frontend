package ro.tuc.ds2020_fe.dtos;

public class MedicationStatus {

	private String name;
	private String status;
	
	public MedicationStatus(String name, String status) {
		super();
		this.name = name;
		this.status = status;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@Override
	public String toString() {
		return "MedicationStatus [name=" + name + ", status=" + status + "]";
	}
	
	
}

