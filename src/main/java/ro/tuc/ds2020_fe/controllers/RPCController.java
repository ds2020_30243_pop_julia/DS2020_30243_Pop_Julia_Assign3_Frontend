package ro.tuc.ds2020_fe.controllers;

import java.io.IOException;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ro.tuc.ds2020_fe.dtos.PatientDTO;
import ro.tuc.ds2020_fe.services.RPCClient;

@RestController
@CrossOrigin("*")
@RequestMapping(value = "/patientRPC")
public class RPCController {

    private final RPCClient rpcClient;

    @Autowired
    public RPCController(RPCClient rpcClient) {
        this.rpcClient = rpcClient;
    }
    
    @PostMapping(value = "/setPatient")
    public ResponseEntity<Integer> setPatientId(@RequestBody String patientId){
        System.out.println(patientId);
        rpcClient.setPatientId(UUID.fromString(patientId));
        return new ResponseEntity<>(1, HttpStatus.OK);
    }
    
    @GetMapping(value = "/downloadMedicationPlan")
    public ResponseEntity<Integer> downloadMedicationPlan() {
		System.out.println("Downloading requested");
    	try {
			rpcClient.downloadMedicationPlan();
		} catch (IOException | InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return new ResponseEntity<>(Integer.parseInt("1"), HttpStatus.OK);
    }
    
    @GetMapping(value = "/takeMedication/{medicationName}")
    public ResponseEntity<Integer> callDoPrint(@PathVariable("medicationName") String medicationName) throws IOException {
        System.out.println(rpcClient.takeMedication(medicationName));
        System.out.println("Got here");

        return new ResponseEntity<>(Integer.parseInt("1"), HttpStatus.OK);
    }
    
    @GetMapping(value = "/addHour")
    public ResponseEntity<Integer> addHour() {
		System.out.println("Add +1");
		rpcClient.addHour();
        return new ResponseEntity<>(Integer.parseInt("1"), HttpStatus.OK);
    }
    
    @GetMapping(value = "/subHour")
    public ResponseEntity<Integer> subHour() {
		System.out.println("Sub -1");
		rpcClient.subHour();
        return new ResponseEntity<>(Integer.parseInt("1"), HttpStatus.OK);
    }
    @GetMapping(value = "/refreshList")
    public ResponseEntity<Integer> refreshList() {
		rpcClient.refreshList();
        return new ResponseEntity<>(Integer.parseInt("1"), HttpStatus.OK);
    }
    
    @GetMapping(value = "/doPrint")
    public ResponseEntity<Integer> callDoPrint() {
        try {
			System.out.println(rpcClient.call("doPrint"));
		} catch (IOException | InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        System.out.println("Got here");

        return new ResponseEntity<>(Integer.parseInt("1"), HttpStatus.OK);
    }
}
