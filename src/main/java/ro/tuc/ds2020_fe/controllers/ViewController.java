package ro.tuc.ds2020_fe.controllers;

import org.jsondoc.core.annotation.ApiMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


import java.util.Date;
import java.util.List;

@Controller
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ViewController {
    private String appMode;

    @Autowired
    public ViewController(Environment environment){
        appMode = environment.getProperty("app-mode");
    }
    
    @RequestMapping("/")
    public String index(Model model){
        model.addAttribute("mode", appMode);
        return "index";
    }
    
    @RequestMapping("/home")
    public String home(Model model){
        model.addAttribute("mode", appMode);
        return "home";
    }

    @RequestMapping("/login")
    public String login(Model model){
        model.addAttribute("mode", appMode);
        return "index";
    }
    
    @RequestMapping("/test")
    public String test(Model model){
        model.addAttribute("mode", appMode);
        return "test";
    }
    
    
    @RequestMapping("/caregivers")
    public String caregivers(Model model) {
    	model.addAttribute("mode", appMode);
    	
    	return "caregivers";
    }
    
    @RequestMapping("/patients")
    public String patients(Model model) {
    	model.addAttribute("mode", appMode);
    	
    	return "patients";
    }
    
    @RequestMapping("/medication")
    public String medication(Model model) {
    	model.addAttribute("mode", appMode);
    	
    	return "medication";
    }
    
    @RequestMapping("/patient_account")
    public String patient(Model model) {
    	model.addAttribute("mode", appMode);
    	
    	return "patient_account";
    }
    
    @RequestMapping("/patientRPC")
    public String pill_dispenser(Model model) {
    	model.addAttribute("mode", appMode);
    	
    	return "pill_dispenser";
    }
    
    
    @RequestMapping("/caregiver_account")
    public String caregiver(Model model) {
    	model.addAttribute("mode", appMode);
    	
    	return "caregiver_account";
    }

}
