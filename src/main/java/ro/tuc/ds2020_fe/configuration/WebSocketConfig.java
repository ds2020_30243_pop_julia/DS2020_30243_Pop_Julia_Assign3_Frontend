package ro.tuc.ds2020_fe.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.config.annotation.AbstractWebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

@Configuration
@EnableWebSocketMessageBroker
@CrossOrigin("*")
public class WebSocketConfig extends AbstractWebSocketMessageBrokerConfigurer {

    @Override
    public void configureMessageBroker(MessageBrokerRegistry config) {
        config.enableSimpleBroker("/topic/", "/queue/");
        config.setApplicationDestinationPrefixes("/pill_dispenser");
    }

    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        System.out.println("Creating endpoint for Websockets");
        registry.addEndpoint("").addInterceptors(new HttpHandshakeInterceptor()).setAllowedOrigins("*");

        registry.addEndpoint("/").addInterceptors(new HttpHandshakeInterceptor()).setAllowedOrigins("*");

        registry.addEndpoint("/pill_dispenser").addInterceptors(new HttpHandshakeInterceptor()).setAllowedOrigins("*");
        //System.out.println(registry.toString());
    }


}