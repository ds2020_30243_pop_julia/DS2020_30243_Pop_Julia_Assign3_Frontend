package ro.tuc.ds2020_fe.services;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import ro.tuc.ds2020_fe.dtos.MedicationPlanDTO;
import ro.tuc.ds2020_fe.dtos.MedicationStatus;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeoutException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Scheduled;

@Service
public class RPCClient implements AutoCloseable {

    private Connection connection;
    private Channel channel;
    private String requestQueueName = "rpc_queue";
    
    private String currentTime = "";
    private Date currentDate = new Date();
    private Date intakeStartDate = new Date();
    private Date intakeEndDate = new Date();
    private Date predefinedDate = new Date();
    private String predefinedTime = "2021-01-13 00:00:00";
    private int hoursOffset = 0;
    
    private UUID patientId = UUID.fromString("ae9e7625-37f5-42c1-bde4-d2f808a802af");
    private MedicationPlanDTO medicationPlan = new MedicationPlanDTO();
    private List<MedicationStatus> medicationsList = new ArrayList<MedicationStatus>();
    private List<MedicationStatus> medicationsListDownloaded = new ArrayList<MedicationStatus>();
    private static boolean missedMedicationFlag = false;
    private static boolean downloadFlag = false;
    
    @Autowired
    private SimpMessagingTemplate template;

    private void calculateAndSendTime() {
    	currentDate = new Date(System.currentTimeMillis());
    	currentDate.setHours(currentDate.getHours() + hoursOffset);
    	currentTime = "";
    	if(currentDate.getHours()<10) currentTime += "0" + currentDate.getHours(); else currentTime += currentDate.getHours();
    	currentTime += ':';
    	if(currentDate.getMinutes()<10) currentTime += "0" + currentDate.getMinutes(); else currentTime += currentDate.getMinutes();
    	currentTime += ':';
    	if(currentDate.getSeconds()<10) currentTime += "0" + currentDate.getSeconds(); else currentTime += currentDate.getSeconds();
    	
        this.template.convertAndSend("/queue/current_time", currentTime);
    }
    
    @Scheduled(fixedRate = 1000)
    public void doProcessing() throws IOException, InterruptedException {
    	calculateAndSendTime();
    	if(currentDate.getHours() == predefinedDate.getHours() &&
    			currentDate.getMinutes() == predefinedDate.getMinutes() &&
    			currentDate.getSeconds() == predefinedDate.getSeconds())
    		downloadMedicationPlan();
    	
    	if(currentDate.after(intakeEndDate)) {
    		missedMedication();
    		refreshList();
    	}
    	if(currentDate.before(intakeStartDate)) {
    		medicationsList = new ArrayList<MedicationStatus>();
    		refreshList();
    	}
    	if(downloadFlag == true && currentDate.before(intakeEndDate) && currentDate.after(intakeStartDate))
    	{
    		medicationsList = medicationsListDownloaded;
    		downloadFlag = false;
    		missedMedicationFlag = false;
    		refreshList();
    	}
    }
    
    private void missedMedication() {
    	if(missedMedicationFlag != true)
    	try {
    	System.out.println("Checking missed medications");
    	System.out.println(medicationsList);
			for(MedicationStatus ms : medicationsList) {
				System.out.println(ms.getStatus());
				System.out.println(ms.getStatus().contentEquals("Taken"));
				
				switch(ms.getStatus()) {
					case "Taken":
						System.out.println("Medication taken: " + ms.getName());
						break;
					default:
						ms.setStatus("Missed");
						System.out.println("Calling missed medication for " + ms.getName());
						this.call("missedMedication+" + ms.getName());	
						break;
				}

			}
			Gson gson = new GsonBuilder().create();
	        String array= gson.toJson(medicationsList, ArrayList.class);
			this.template.convertAndSend("/queue/medicationsList", array);
			
			 refreshList();
			missedMedicationFlag = true;
		} catch (IOException | InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	public void setPatientId(UUID patientId) {
    	this.patientId = patientId;
    }
        
    public void downloadMedicationPlan() throws IOException, InterruptedException {
    	
    	intakeStartDate = new Date(System.currentTimeMillis());
    	intakeEndDate = new Date(System.currentTimeMillis());
    	
    	System.out.println("Downloading medication plan");
    	medicationPlan = medicationCall("medicationPlan+" + patientId);
    	
    	String[] intervals = medicationPlan.getIntakeIntervals().split("\\-");
    	
    	String[] startInterval = intervals[0].split(":");
    	intakeStartDate.setHours(Integer.parseInt(startInterval[0]));
    	intakeStartDate.setMinutes(Integer.parseInt(startInterval[1]));
    	intakeStartDate.setSeconds(Integer.parseInt(startInterval[2]));
    	
    	String[] endInterval = intervals[1].split(":");
    	intakeEndDate.setHours(Integer.parseInt(endInterval[0]));
    	intakeEndDate.setMinutes(Integer.parseInt(endInterval[1]));
    	intakeEndDate.setSeconds(Integer.parseInt(endInterval[2]));  	
    	
    	this.template.convertAndSend("/queue/intakeInterval", medicationPlan.getIntakeIntervals());
    	
    	System.out.println(medicationPlan);
    	medicationsListDownloaded = new ArrayList<MedicationStatus>();
    	
	    for(String medication : medicationPlan.getMedicationNames())
	    	medicationsListDownloaded.add(new MedicationStatus(medication,"Not taken"));
	    
	    downloadFlag = true;
    	System.out.println(medicationsListDownloaded.toString());
    	
    	Gson gson = new GsonBuilder().create();
        String array= gson.toJson(medicationsList, ArrayList.class);
        
        missedMedicationFlag = false;
        
    	this.template.convertAndSend("/queue/medicationsList", array);
    	refreshList();
    }
    
    public RPCClient() throws IOException, TimeoutException, ParseException {
    	  ConnectionFactory factory = new ConnectionFactory();
          factory.setHost("sparrow.rmq.cloudamqp.com");
          factory.setUsername("boyrjkey");
          factory.setPassword("Qyw89sPcD_hNivow241cOH11AT3-5kSe");
          factory.setVirtualHost("boyrjkey");
          
        connection = factory.newConnection();
        channel = connection.createChannel();
        predefinedDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(predefinedTime);
    }

    public String call(String message) throws IOException, InterruptedException {
        final String corrId = UUID.randomUUID().toString();

        String replyQueueName = channel.queueDeclare().getQueue();
        AMQP.BasicProperties props = new AMQP.BasicProperties
                .Builder()
                .correlationId(corrId)
                .replyTo(replyQueueName)
                .build();

        channel.basicPublish("", requestQueueName, props, message.getBytes("UTF-8"));

        final BlockingQueue<String> response = new ArrayBlockingQueue<>(1);

        String ctag = channel.basicConsume(replyQueueName, true, (consumerTag, delivery) -> {
            if (delivery.getProperties().getCorrelationId().equals(corrId)) {
                response.offer(new String(delivery.getBody(), "UTF-8"));
            }
        }, consumerTag -> {
        });

        String result = response.take();
        channel.basicCancel(ctag);
        return result;
    }
    
    public MedicationPlanDTO medicationCall(String message) throws IOException, InterruptedException {
        final String corrId = UUID.randomUUID().toString();

        String replyQueueName = channel.queueDeclare().getQueue();
        AMQP.BasicProperties props = new AMQP.BasicProperties
                .Builder()
                .correlationId(corrId)
                .replyTo(replyQueueName)
                .build();

        channel.basicPublish("", requestQueueName, props, message.getBytes("UTF-8"));

        final BlockingQueue<String> response = new ArrayBlockingQueue<>(1);

        String ctag = channel.basicConsume(replyQueueName, true, (consumerTag, delivery) -> {
            if (delivery.getProperties().getCorrelationId().equals(corrId)) {
                response.offer(new String(delivery.getBody(), "UTF-8"));
            }
        }, consumerTag -> {
        });
        
        Gson gson = new GsonBuilder().create();
        MedicationPlanDTO result = gson.fromJson(response.take(), MedicationPlanDTO.class);
        
        channel.basicCancel(ctag);
        
        return result;
    }
    public void close() throws IOException {
        connection.close();
    }

	public String takeMedication(String medicationName) {
		
		try {
			for(MedicationStatus ms : medicationsList) {
				if(ms.getName().contentEquals(medicationName))
					ms.setStatus("Taken");		
			}		
			this.call("takeMedication+" + medicationName);
			Gson gson = new GsonBuilder().create();
	        String array= gson.toJson(medicationsList, ArrayList.class);
			this.template.convertAndSend("/queue/medicationsList", array);
		} catch (IOException | InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "Medication taken successfully";
	}
	public void refreshList() {
		Gson gson = new GsonBuilder().create();
        String array= gson.toJson(medicationsList, ArrayList.class);
		this.template.convertAndSend("/queue/medicationsList", array);
		
	}
	
    public void addHour() {
    	hoursOffset++;
    }
    public void subHour() {
    	hoursOffset--;
    }
}