(function () {
    'use strict';

    angular
    	.module('app', ['ngStorage'])
        .controller('DoctorController', DoctorController);

    DoctorController.$inject = ['$scope', '$localStorage', '$http', '$window', '$location'];

    function DoctorController($scope, $localStorage, $http, $window, $location) {
        var vm = this;
        
        vm.loggedDoctor = {};
        
        $scope.data = {};
        $localStorage.data = {};			
        
        $scope.$storage = $localStorage;
        $scope.loggedAccount = $localStorage.loggedAccount;

        console.log($scope.loggedAccount.ownerId);
        if($scope.loggedAccount.ownerId != undefined){
	        if(!$scope.loggedAccount.type.localeCompare("doctor")){
				console.log("Logged in as Doctor#" + $scope.loggedAccount.ownerId);
	        }
	        else if(!$scope.loggedAccount.type.localeCompare("patient")){
	        	$window.location.href = "/patient_account";
	        }
	        else if(!$scope.loggedAccount.type.localeCompare("caregiver")){
	        	$window.location.href = "/caregiver_account";
	        }
	        else{
	        	$window.location.href = "/login";
	        }
        }
        else
        {
        	//$window.alert("Please log in to continue.");
        	$window.location.href = "/login";
        }
        
        /* Functii patient*/
        vm.findAllPatients = findAllPatients;
        vm.findAllMedications = findAllMedications;
        vm.findAllCaregivers = findAllCaregivers;
        vm.addPatient = addPatient;
        vm.updatePatient = updatePatient;
        vm.updateSelectedPatientJSON = updateSelectedPatientJSON;
        vm.updateSelectedPatient = updateSelectedPatient;
        vm.deletePatient = deletePatient;
        vm.addAccount = addAccount;
        vm.addCaregiver = addCaregiver;
        vm.updateCaregiver = updateCaregiver;
        vm.updateSelectedCaregiverJSON = updateSelectedCaregiverJSON;
        vm.deleteCaregiver = deleteCaregiver;
        vm.addCaregiverAccount = addCaregiverAccount;
        vm.addMedication = addMedication;
        vm.deleteMedication = deleteMedication;
        vm.updateSelectedMedicationJSON = updateSelectedMedicationJSON;
        vm.updateMedication = updateMedication;
        vm.previewMedicationPlan = previewMedicationPlan;
        vm.createMedicationPlan = createMedicationPlan;
        vm.addToMedicationPlan = addToMedicationPlan;
        vm.search = search;
  
        /* Liste*/
        vm.allPatients = [];
        vm.allMedications = [];
        vm.allCaregivers = [];
       
       	/* Obiecte*/ 
        vm.selectedPatient = {		
        						"id":null,
        		 				"name":"",
						        "birthDate":"",
						        "gender":"",
						        "address":"",
						        "medicalRecord":"",
						        "caregiverName":""
	        	  		}
        vm.selectedMedication = {		
        						"id":null,
        		 				"name":"",
						        "sideEffects":"",
						        "dosage":""};
        vm.selectedCaregiver = {		
        						"id":null,
        		 				"name":"",
						        "birthDate":"",
						        "gender":"",
						        "address":""
						        };
        
        /* MedicationPlan */

        vm.medicationPlanInstance = {
        	"intakeIntervals" : "",
    		"treatmentPeriod" : "",
    		"doctorName" : "",
    		"patientName" : "",
    		"medicationNames" : []
   		};
             
   
        init();

        function init(){
        	
        	findAllPatients();
        	findAllCaregivers();
        	findAllMedications();
        }
        
        /* Implementare functii */
        
        function search(searchString, type){
        	console.log("Searching for a " + type);
        	if(!searchString)
        	{
        		if(!type.localeCompare("Patient"))
	                		findAllPatients();
	                	else if(!type.localeCompare("Caregiver"))
	                		findAllCaregivers();
	                	else if(!type.localeCompare("Medication"))
	                		findAllMedications();
        	}
        	else if(!searchString.localeCompare(""))
        	{
                		if(!type.localeCompare("Patient"))
	                		findAllPatients();
	                	else if(!type.localeCompare("Caregiver"))
	                		findAllCaregivers();
	                	else if(!type.localeCompare("Medication"))
	                		findAllMedications();
               }
        	else{
	        	$http({
	        		method: 'GET',
	        		url: 'https://medical-platform-ds2020.herokuapp.com/doctor/search' + type +'/'+ searchString
	        		}).then(function success(response) {
	
	                	console.log("Request sent sucessfully.\n");
	                	console.log(response.data)
	                	if(!type.localeCompare("Patient"))
	                		vm.allPatients = response.data;
	                	else if(!type.localeCompare("Caregiver"))
	                		vm.allCaregivers = response.data
	                	else if(!type.localeCompare("Medication"))
	                		vm.allMedications = response.data
	                		
	                	//$window.location.href("");
	
	        		}, function error(response) {
	        			console.log("Test error.");
	        		});
        	}
        }
        
        function addToMedicationPlan(medicationName){
        	if(!vm.medicationPlanInstance.medicationNames.includes(medicationName))
        		vm.medicationPlanInstance.medicationNames.push(medicationName);
        	else
        		$window.alert("Already added");
        }
        
        function previewMedicationPlan(){
        	var medicationsString = "";
        	for(var med of vm.medicationPlanInstance.medicationNames)
        		medicationsString = medicationsString + med + '\n';
        	$window.alert("Selected medications:\n" + medicationsString);
        }
        function createMedicationPlan(patientName, intakeTxt, periodTxt){
        
        	var url = "https://medical-platform-ds2020.herokuapp.com/doctor/createMedicationPlan";
        	
        	vm.medicationPlanInstance.intakeIntervals = intakeTxt;
        	vm.medicationPlanInstance.treatmentPeriod = periodTxt;
        	vm.medicationPlanInstance.patientName = patientName;
        	vm.medicationPlanInstance.doctorName = $scope.loggedAccount.username;
        	
        	console.log(vm.medicationPlanInstance);
	      	
	      $http.post(url, vm.medicationPlanInstance).then(function (response)  {
	
	        	console.log("Request sent sucessfully.");
	        	
	        	        vm.medicationPlanInstance = {
						        	"intakeIntervals" : "",
						    		"treatmentPeriod" : "",
						    		"doctorName" : "",
						    		"patientName" : "",
						    		"medicationNames" : []
						   		};
	        	
	        	}, function (response) {
	        		
	        		console.log("Test error.");
	
	        	});
	        	
        }
        function findAllPatients(){
        	console.log("Retrieving patients list.\n");

        	$http({
        		method: 'GET',
        		url: 'https://medical-platform-ds2020.herokuapp.com/doctor/findAllPatients',
        		}).then(function success(response) {
        		
                	console.log("Request sent sucessfully.\n Response:");
                	console.log(response.data)
                	
                	vm.allPatients =response.data;

        		}, function error(response) {
        			console.log("Request error.");
        		});
        }

        
        function findAllMedications(){
        console.log("Retrieving medications list.\n");

        	$http({
        		method: 'GET',
        		url: 'https://medical-platform-ds2020.herokuapp.com/doctor/findAllMedications',
        		}).then(function success(response) {
        		
                	console.log("Request sent sucessfully.\n Response:");
                	console.log(response.data)
                	
                	vm.allMedications =response.data;

        		}, function error(response) {
        			console.log("Request error.");
        		});
        }
        
        function findAllCaregivers(){
        console.log("Retrieving caregivers list.\n");

        	$http({
        		method: 'GET',
        		url: 'https://medical-platform-ds2020.herokuapp.com/doctor/findAllCaregivers',
        		}).then(function success(response) {
        		
                	console.log("Request sent sucessfully.\n Response:");
                	console.log(response.data)
                	
                	vm.allCaregivers = response.data;

        		}, function error(response) {
        			console.log("Request error.");
        		});
        }
        
        function addPatient(nameTxt, birthDateTxt, genderTxt, addressTxt, medicalRecordTxt, caregiverName){
        	var url = "https://medical-platform-ds2020.herokuapp.com/doctor/createPatient";
	        var data = {		"name":nameTxt,
						        "birthDate":birthDateTxt,
						        "gender":genderTxt,
						        "address":addressTxt,
						        "medicalRecord":medicalRecordTxt,
						        "caregiverName":caregiverName
	        	  		}
	        	  		
	        console.log(data);
	      	
	      $http.post(url, data).then(function (response)  {
	
	        	console.log("Request sent sucessfully.");
	        	console.log(response.data);
	        	$window.location.href = "/patients";
	        	
	        	}, function (response) {
	        		
	        		console.log("Test error.");
	
	        	});
	        	
        	
        	}
        	
        function addCaregiver(nameTxt, birthDateTxt, genderTxt, addressTxt){
        	var url = "https://medical-platform-ds2020.herokuapp.com/doctor/createCaregiver";
	        var data = {		"name":nameTxt,
						        "birthDate":birthDateTxt,
						        "gender":genderTxt,
						        "address":addressTxt
	        	  		}
	        	  		
	        console.log(data);
			
	      $http.post(url, data).then(function (response)  {
	
	        	console.log("Request sent sucessfully.");
	        	console.log(response.data);
	        	$window.location.href = "/caregivers";
	        	
	        	}, function (response) {
	        		
	        		console.log("Test error.");
	
	        	});
	        	
        
        	}
        	
      function updatePatient(nameTxt, birthDateTxt, genderTxt, addressTxt, medicalRecordTxt, caregiverName){
        	var url = "https://medical-platform-ds2020.herokuapp.com/doctor/updatePatient";
        	console.log("UPDATING:");
        	if(nameTxt != undefined)
	       		vm.selectedPatient.name = nameTxt;
	       	if(birthDateTxt != undefined)
	       		vm.selectedPatient.birthDate = birthDateTxt; 		
	       	if(genderTxt != undefined)
	       		vm.selectedPatient.gender = genderTxt;
	       	if(addressTxt != undefined)
	       		vm.selectedPatient.address = addressTxt;
	       	if(medicalRecordTxt != undefined)
	       		vm.selectedPatient.medicalRecord = medicalRecordTxt;
	       	if(caregiverName != undefined)
	       		vm.selectedPatient.caregiverName = caregiverName;
	        	  		
	        console.log(vm.selectedPatient);
			 $http.put(url, vm.selectedPatient).then(function (response)  {
	
	        	console.log("Request sent sucessfully.");
	        	console.log(response.data);
	        	
	        	}, function (response) {
	        		
	        		console.log("Test error.");
	
	        	});
	        
        	}
        	
        	function updateSelectedPatient(id, nameTxt, birthDateTxt, genderTxt, addressTxt, medicalRecordTxt, caregiverName){
        		 	vm.selectedPatient.id = id;
        		 	vm.selectedPatient.name = nameTxt;
			       	vm.selectedPatient.birthDate = birthDateTxt;
			       	vm.selectedPatient.gender = genderTxt;
			       	vm.selectedPatient.address = addressTxt;
			       	vm.selectedPatient.medicalRecord = medicalRecordTxt;
			       	vm.selectedPatient.caregiverName = caregiverName;
        		 	console.log(vm.selectedPatient);
        	}
        	function updateSelectedPatientJSON(patientJSON){
        		 	vm.selectedPatient = patientJSON;
        			$('#exampleModalUpdatePatient');
        			document.getElementById('exampleModalUpdatePatient').display='none';
        			document.getElementById('exampleModalUpdatePatient').display='block';
        		 	console.log(vm.selectedPatient);
        	}

        	function deletePatient(id){
        	var url = "https://medical-platform-ds2020.herokuapp.com/doctor/deletePatient/" + id;
			 $http.delete(url).then(function (response)  {
	
	        	console.log("Request sent sucessfully.");
	        	console.log(response.data);
	        	
	        	 $window.location.href = "/patients";
	        	
	        	}, function (response) {
	        		
	        		console.log("Test error.");
	
	        	});
	        	
        }
        
        function updateCaregiver(nameTxt, birthDateTxt, genderTxt, addressTxt){
        	var url = "https://medical-platform-ds2020.herokuapp.com/doctor/updateCaregiver";
        	console.log("UPDATING:");
        	if(nameTxt != undefined)
	       		vm.selectedCaregiver.name = nameTxt;
	       	if(birthDateTxt != undefined)
	       		vm.selectedCaregiver.birthDate = birthDateTxt; 		
	       	if(genderTxt != undefined)
	       		vm.selectedCaregiver.gender = genderTxt;
	       	if(addressTxt != undefined)
	       		vm.selectedCaregiver.address = addressTxt;

	        console.log(vm.selectedCaregiver);
			 $http.put(url, vm.selectedCaregiver).then(function (response)  {
	
	        	console.log("Request sent sucessfully.");
	        	console.log(response.data);
	        	
	        	}, function (response) {
	        		
	        		console.log("Test error.");
	
	        	});
	        
        	}
        	
        	function updateSelectedCaregiverJSON(caregiverJSON){
        		 	vm.selectedCaregiver = caregiverJSON;
        			$('#exampleModalUpdateCaregiver');
        			document.getElementById('exampleModalUpdateCaregiver').display='none';
        			document.getElementById('exampleModalUpdateCaregiver').display='block';
        		 	console.log(vm.selectedCaregiver);
        	}
        
        function deleteCaregiver(id){
        	var url = "https://medical-platform-ds2020.herokuapp.com/doctor/deleteCaregiver/" + id;
			 $http.delete(url).then(function (response)  {
	
	        	console.log("Request sent sucessfully.");
	        	console.log(response.data);
	        	
	        	 $window.location.href = "/caregivers";
	        	
	        	}, function (response) {
	        		
	        		console.log("Test error.");
	
	        	});
	        	
        }
        
        function addAccount(username, password, patientName){
        var url = "https://medical-platform-ds2020.herokuapp.com/doctor/createAccount";
        var data = {		
        	"username":username,
			"password":password,
			"ownerId":null,
			"type":"patient"
	    }
	    console.log(patientName);
        for(var patient of vm.allPatients){
        	 console.log(patient.name);
        	if(!patient.name.localeCompare(patientName))
        		data.ownerId = patient.id;
        }
   	  		
	    console.log(data);
	      $http.post(url, data).then(function (response)  {
	
	        	console.log("Request sent sucessfully.");
	        	console.log(response.data);
	        	
	        	}, function (response) {
	        		
	        		console.log("Test error.");
	
	        	});
	        	
        }
        
        function addCaregiverAccount(username, password, caregiverName){
	        var url = "https://medical-platform-ds2020.herokuapp.com/doctor/createAccount";
	        var data = {		
	        	"username":username,
				"password":password,
				"ownerId":null,
				"type":"caregiver"
		    }
		    console.log(caregiverName);
	        for(var caregiver of vm.allCaregivers){
	        	 console.log(caregiver.name);
	        	if(!caregiver.name.localeCompare(caregiverName))
	        		data.ownerId = caregiver.id;
	        }
	   	  		
		    console.log(data);
		      $http.post(url, data).then(function (response)  {
		
		        	console.log("Request sent sucessfully.");
		        	console.log(response.data);
		        	
		        	}, function (response) {
		        		
		        		console.log("Test error.");
		
		        	});
	        	
        }
        
        function addMedication(nameTxt, sideEffectsTxt, dosageTxt){
        	var url = "https://medical-platform-ds2020.herokuapp.com/doctor/addMedication";
	        var data = {		"name":nameTxt,
						        "sideEffects":sideEffectsTxt,
						        "dosage":dosageTxt
	        	  		}
	        	  		
	        console.log(data);
			
	      $http.post(url, data).then(function (response)  {
	
	        	console.log("Request sent sucessfully.");
	        	console.log(response.data);
	        	$window.location.href = "/medication";
	        	
	        	}, function (response) {
	        		
	        		console.log("Test error.");
	
	        	});
	        	
        
        	}
        	
        	
        function updateMedication(nameTxt, sideEffectsTxt, dosageTxt){
        	var url = "https://medical-platform-ds2020.herokuapp.com/doctor/updateMedication";
        	console.log("UPDATING:");
        	if(nameTxt != undefined)
	       		vm.selectedMedication.name = nameTxt;
	       	if(sideEffectsTxt != undefined)
	       		vm.selectedMedication.sideEffects = sideEffectsTxt; 		
	       	if(dosageTxt != undefined)
	       		vm.selectedMedication.dosage = dosageTxt;

	        console.log(vm.selectedMedication);
			 $http.put(url, vm.selectedMedication).then(function (response)  {
	
	        	console.log("Request sent sucessfully.");
	        	console.log(response.data);
	        	
	        	}, function (response) {
	        		
	        		console.log("Test error.");
	
	        	});
	        
        	}
        	
        	function updateSelectedMedicationJSON(medicationJSON){
        		 	vm.selectedMedication = medicationJSON;
        		 	console.log(vm.selectedMedication);
        	}
        	
        function deleteMedication(id){
        	var url = "https://medical-platform-ds2020.herokuapp.com/doctor/deleteMedication/" + id;
			 $http.delete(url).then(function (response)  {
	
	        	console.log("Request sent sucessfully.");
	        	console.log(response.data);
	        	
	        	 $window.location.href = "/medication";
	        	
	        	}, function (response) {
	        		
	        		console.log("Test error.");
	
	        	});
	        	
        }
        /* End of Implementare functii */ 
    }
})();
