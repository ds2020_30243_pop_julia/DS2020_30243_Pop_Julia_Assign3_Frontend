(function () {
    'use strict';

    angular
    .module('app', ['ngStorage'])
    .controller('IndexController', IndexController);

    IndexController.$inject = ['$scope', '$localStorage', '$http', '$window', '$location'];

    function IndexController($scope, $localStorage, $http, $window, $location) {
        var vm = this;

        $scope.data ={};
        $localStorage.data = {};
		
        $scope.$storage = $localStorage;
        
        $localStorage.loggedAccount = {};
        $scope.loggedAccount = {};
        
        vm.login = login;
        vm.testAll = testAll;
        init();

        function init(){
        	
        }
        
        function testAll(){
        $http({
        		method: 'GET',
        		url: 'https://medical-platform-ds2020.herokuapp.com/doctor/testAll',
        		}).then(function success(response) {
        		
                	console.log("Request sent sucessfully.");

        		}, function error(response) {
        			console.log("Request error.");
        		});
        }

        function login(username, password){
        	console.log("\n Verifying login: " + username);
        
        		
	        	var url = 'https://medical-platform-ds2020.herokuapp.com/index/login';
	        	var data = {		"id":null,
						        	"username": username,
						        	"password":password,
						        	"ownerId": null,
						        	"type": ""
	        	  			}
	
	        	$http.post(url, data).then(function success(response) {
	
	        	console.log("Request sent sucessfully.");
	        	console.log(response.data);
	        	
	        	if( !response.data.password.localeCompare(password) && 
	        		!response.data.username.localeCompare(username)	)
	        				{
		        				console.log("Login sent sucessfully.");

		                        $localStorage.loggedAccount = response.data;
		                        $scope.loggedAccount = $localStorage.loggedAccount;
		                               	
		                    	console.log($scope.loggedAccount);
		                    	if(!response.data.type.localeCompare("doctor"))
		                    		$window.location.href = '/home';
		                    	else if(!response.data.type.localeCompare("patient"))
		                    		$window.location.href = '/patient_account';
		                    	else if(!response.data.type.localeCompare("caregiver"))
		                    		$window.location.href = '/caregiver_account';
	        				
	        				}
	        			else
		        			{
		        				$window.alert("Invalid password")
		        			}
	        	
	
	        	}, function success(response) {
			
	        		console.log("Test error.");
		            $window.alert("Account not found")
	
	        	});
                	
        }       
    }
})();
