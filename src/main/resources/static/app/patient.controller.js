(function () {
    'use strict';

    angular
    	.module('app', ['ngStorage'])
        .controller('PatientController', PatientController);

    PatientController.$inject = ['$scope', '$localStorage', '$http', '$window', '$location'];

    function PatientController($scope, $localStorage, $http, $window, $location) {
        var vm = this;
        
        vm.loggedPatient = {};
        vm.connect = connect;
        $scope.data = {};
        $localStorage.data = {};			
        
        $scope.$storage = $localStorage;
        $scope.loggedAccount = $localStorage.loggedAccount;

        console.log($scope.loggedAccount.ownerId);
        if($scope.loggedAccount.ownerId != undefined){
	        if(!$scope.loggedAccount.type.localeCompare("doctor")){
				console.log("Logged in as Doctor#" + $scope.loggedAccount.ownerId);
	        	$window.location.href = "/home";
	        }
	        else if(!$scope.loggedAccount.type.localeCompare("patient")){
	        	console.log("Logged in as Patient#" + $scope.loggedAccount.ownerId);
	        	  $http.post('http://localhost:8081/patientRPC/setPatient', '' + $scope.loggedAccount.ownerId).then(function (response)  {
					connect();
	        		console.log("Request sent sucessfully.");
	        	
	        	}, function (response) {
	        		
	        		console.log("Test error.");
	
	        	});
	        	//$window.location.href = "/patient_account";
	        }
	        else if(!$scope.loggedAccount.type.localeCompare("caregiver")){
	        	$window.location.href = "/caregiver_account";
	        }
	        else{
	        	$window.location.href = "/login";
	        }
        }
        else
        {
        	//$window.alert("Please log in to continue.");
        	$window.location.href = "/login";
        }
        
        /* Functii patient*/
        vm.getPatient = getPatient;
        vm.rpc_doPrint = rpc_doPrint;
		vm.download = download;
		vm.takeMedication = takeMedication;
		vm.refreshList = refreshList;
		
        /* Liste*/
        vm.patient = {};
        vm.medicationsList = [];
	  	var ws = null;
	  	
        init();
        

        function init(){
        	
        	getPatient();
        }
        
        /* Implementare functii */
        function getPatient(){
         	$http({
        		method: 'GET',
        		url: 'https://medical-platform-ds2020.herokuapp.com/patient/getPatient/' + $scope.loggedAccount.username,
        		}).then(function success(response) {
        		
                	console.log("Request sent sucessfully.\n Response:");
                	console.log(response.data)
                	
                	vm.patient = response.data;

        		}, function error(response) {
        			console.log("Request error.");
        		});
        }
        function download(){
       		$http({
        		method: 'GET',
        		url: 'http://localhost:8081/patientRPC/downloadMedicationPlan',
        		}).then(function success(response) {
        		
                	console.log("Request sent sucessfully.\n Response:");
                	console.log(response.data)

        		}, function error(response) {
        			console.log("Request error.");
        		});
        }
        
        function connect() {
        $http({
        		method: 'GET',
        		url: 'http://localhost:8080/rpc/start',
        		}).then(function success(response) {
        		
                	console.log("Request sent sucessfully.\n Response:");
                	console.log(response.data)
                	
                	vm.patient = response.data;

        		}, function error(response) {
        			console.log("Request error.");
        		});
        		
			//var socket = new SockJS('http://localhost:8080/caregiver/greeting');
			//var socket = new WebSocket('ws://medical-platform-ds2020.herokuapp.com/greeting', 'echo-protocol');		
			var socket = new WebSocket('ws://localhost:8081/pill_dispenser', 'echo-protocol');		
			ws = Stomp.over(socket);
		
			ws.connect({}, function(frame) {
				ws.subscribe("/queue/medicationsList", function(message) {
					updateMedicationsList(message.body);
				});
				ws.subscribe("/queue/current_time", function(message) {
					showCurrentTime(message.body);
				});
				ws.subscribe("/queue/intakeInterval", function(message) {
					showIntakeInterval(message.body);
				});
			}, function(error) {
				alert("Could not connect to web socket: " + error);
			});
			
		}
	
		function showCurrentTime(message) {
		    $("#currentTime").text(" " + message + '\n');
		}
		function showIntakeInterval(message) {
		    $("#intakeInterval").text(" " + message + '\n');
		}
		function updateMedicationsList(message) {
			console.log(message);
			vm.medicationsList = JSON.parse(message);
			console.log(vm.medicationsList);
			console.log(vm.medicationsList.length);
	
		    //$("#medicationsTable").text("");
		    	
		    $("#medicationsTable").load();
		    $('#refreshDiv').load(location.href +  ' #refreshDiv');
		}
		
        function refreshList(){
          $http({
        		method: 'GET',
        		url: 'http://localhost:8081/patientRPC/refreshList',
        		}).then(function success(response) {
        		
                	console.log("Request sent sucessfully.\n Response:");
                	console.log(response.data)
                	
                	vm.patient = response.data;

        		}, function error(response) {
        			console.log("Request error.");
        		});
        }
        
        function rpc_doPrint(){      	
       		$http({
        		method: 'GET',
        		url: 'http://localhost:8081/patientRPC/doPrint',
        		}).then(function success(response) {
        		
                	console.log("Request sent sucessfully.\n Response:");
                	console.log(response.data)

        		}, function error(response) {
        			console.log("Request error.");
        		});
        }
        
        function takeMedication(medicationName){
       		console.log(medicationName);
       		$http({
        		method: 'GET',
        		url: 'http://localhost:8081/patientRPC/takeMedication/' + medicationName,
        		}).then(function success(response) {
        		
                	console.log("Request sent sucessfully.\n Response:");
                	console.log(response.data)

        		}, function error(response) {
        			console.log("Request error.");
        		});
        }
        
        }
        
        /* End of Implementare functii */ 

    
})();
