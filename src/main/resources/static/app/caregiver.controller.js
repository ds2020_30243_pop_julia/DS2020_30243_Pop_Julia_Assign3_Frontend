(function () {
    'use strict';

    angular
    	.module('app', ['ngStorage'])
        .controller('CaregiverController', CaregiverController);

    CaregiverController.$inject = ['$scope', '$localStorage', '$http', '$window', '$location'];

    function CaregiverController($scope, $localStorage, $http, $window, $location) {
        var vm = this;
        
        vm.loggedCaregiver = {};
        
        $scope.data = {};
        $localStorage.data = {};			
        
        $scope.$storage = $localStorage;
        $scope.loggedAccount = $localStorage.loggedAccount;

		var ws = null;
		
        /* Functii patient*/
        vm.getCaregiver = getCaregiver;
        vm.startConsumer = startConsumer;
        vm.stopConsumer = stopConsumer;
        vm.connect = connect;
		
		vm.stopConsumer(); 
		
        /* Liste*/
        vm.caregiver = {};
        
        console.log($scope.loggedAccount.ownerId);
        if($scope.loggedAccount.ownerId != undefined){
	        if(!$scope.loggedAccount.type.localeCompare("doctor")){
				console.log("Logged in as Doctor#" + $scope.loggedAccount.ownerId);
	        	$window.location.href = "/home";
	        }
	        else if(!$scope.loggedAccount.type.localeCompare("patient")){
	        	console.log("Logged in as Patient#" + $scope.loggedAccount.ownerId);
	        	$window.location.href = "/patient_account";
	        }
	        else if(!$scope.loggedAccount.type.localeCompare("caregiver")){
	       	 	  console.log("Logged in as Caregiver#" + $scope.loggedAccount.ownerId);
	       	 	  vm.startConsumer();       	 	  
        		  vm.connect();
	        	//$window.location.href = "/caregiver_account";
	        }
	        else{
	        	$window.location.href = "/login";
	        }
        }
        else
        {
        	//$window.alert("Please log in to continue.");
        	$window.location.href = "/login";
        }

		
		
        init();

        function init(){
        	
        	getCaregiver();
        }
        
		function connect() {
			//var socket = new SockJS('http://localhost:8080/caregiver/greeting');
			//var socket = new WebSocket('ws://medical-platform-ds2020.herokuapp.com/greeting', 'echo-protocol');		
			var socket = new WebSocket('wss://medical-platform-ds2020.herokuapp.com/greeting', 'echo-protocol');		
			ws = Stomp.over(socket);
		
			ws.connect({}, function(frame) {
				ws.subscribe("queue/errors", function(message) {
					alert("Error " + message.body);
				});
		
				ws.subscribe("/queue/reply", function(message) {
					showGreeting(message.body);
				});
			}, function(error) {
				alert("Could not connect to web socket: " + error);
			});
			
		}
	
		function showGreeting(message) {
		    $("#greetings").append(" " + message + '\n');
		}

        
        /* Implementare functii */
        function getCaregiver(){
         	$http({
        		method: 'GET',
        		url: 'https://medical-platform-ds2020.herokuapp.com/caregiver/getCaregiver/' + $scope.loggedAccount.username,
        		}).then(function success(response) {
        		
                	console.log("Request sent sucessfully.\n Response:");
                	console.log(response.data)
                	
                	vm.caregiver = response.data;

        		}, function error(response) {
        			console.log("Request error.");
        		});
        }
        function startConsumer(){
        //url: 'https://medical-platform-ds2020.herokuapp.com/caregiver/startConsumer/localhost/EXCHANGE_NAME',
        //url: 'http://localhost:8080/caregiver/startConsumer/localhost/EXCHANGE_NAME',
         	$http({
        		method: 'GET',
        		url: 'https://medical-platform-ds2020.herokuapp.com/caregiver/startConsumer/localhost/EXCHANGE_NAME',
        		}).then(function success(response) {
        		
                	console.log("Request sent sucessfully.\n Response:");
                	console.log(response.data)
                	
                	vm.caregiver = response.data;

        		}, function error(response) {
        			console.log("Request error.");
        		});
        }
        
         function stopConsumer(){
         	$http({
        		method: 'GET',
        		url: 'https://medical-platform-ds2020.herokuapp.com/caregiver/stopConsumer/localhost/EXCHANGE_NAME',
        		}).then(function success(response) {
        		
                	console.log("Request sent sucessfully.\n Response:");
                	console.log(response.data)
                	
                	vm.caregiver = response.data;

        		}, function error(response) {
        			console.log("Request error.");
        		});
        }

        /* End of Implementare functii */ 
    }
})();
/*		
		$(function() {
			$("form").on('submit', function(e) {
				e.preventDefault();
			});
			$("#connect").click(function() {
				connect();
			});
			$("#disconnect").click(function() {
				disconnect();
			});
			$("#send").click(function() {
				sendName();
			});
		});
*/
/*		
		function disconnect() {
		    if (ws != null) {
		        ws.close();
		    }
		    setConnected(false);
		    console.log("Disconnected");
		}
		
		function sendName() {
			var data = JSON.stringify({
				'name' : $("#name").val()
			})
			ws.send("/caregiver/message", {}, data);
		}
*/